<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<title>SPUP</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>		


			<!-- Page Title -->
			<div class="container" style="margin-top: 0px">
	 	 		<div class="jumbotron">
	 		 		<img src="image/spuplogo.png" alt="" style="height: 100px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h3 style="text-align: center">St. Paul University Philippines</h3>
	  			</div>	
			</div>

			<!-- Menu Bar -->
			<div class="container" style="margin-top: -30px">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid" >
				    <div class="navbar-header">
				    </div>
				    <ul class="nav navbar-nav">
				      <li class="active"><a href="index.php">Home</a></li>
				      <li><a href="history.php">History</a></li>
				      <li><a href="#">Vision-Mission and Quality Policies</a></li>
				      <li><a href="#">Core Value</a></li>
				      <li><a href="#">Paulinian Hymn</a></li>
				    </ul>
				  </div>
				</nav>
			</div>
		



	<div class="container">
		<div class="container-fluid">
		<h1 style="text-align: center;border-style: solid;border-color: #87ceeb">SPUP through the Years</h1>
		<h4><img src="image/history1.jpg" alt="" style="height: 100px;width: 200px;float:left;display:block; margin-left: auto; margin-right: 5px;">
		St. Paul University Philippines (SPUP) was founded on May 10, 1907, as Colegio de San Pablo, by the Sisters of St. Paul of Chartres, (Mother Ephrem Marie Fieu, Sr. Suzanne du Sacre Coeur Cran, Sr. Agnes de Sainte Anne Couplan, and Sr. Jeanne de St. Louis Bourrelly, Sr. Marie Angeline Acau and Postulant Sr. Ste. Foy de Sacre Coeur Sacramento) who came to Cagayan Valley upon the invitation of Bishop Dennis Dougherty. The school’s name was changed to Colegio del Sagrado Corazon de Jesus in 1909, then to Sacred Heart of Jesus Institution (SHOJI) in 1925. The school started in a Spanish Convento, adjoining the Cathedral, but due to increase of enrollment and curricular expansion, the community moved to the Colegio de San Jacinto and its grounds in 1934 (where the University stands today) which the SPC Sisters acquired from the Dominican Order.</h4>


		<h4><img src="image/history2.jpg" alt="" style="height: 100px;width: 200px;float:right;display:block; margin-left: auto; margin-right: auto;">
		The school served as military garrison and hospital of the Japanese forces in 1941. The entire complex was razed to the ground during the Liberation. In 1948, the school undertook the initiative of being the first Teacher-Training Institution in Cagayan Valley, as it assumed the name St. Paul College of Tuguegarao (SPCT). With the opening of college degrees, the school became the first accredited institution in the region by the Philippine Accrediting Association of Schools, Colleges and Universities (PAASCU) in 1961. However, the entire complex was reduced into ashes on January 18, 1965. Despite the challenges and like a gold tested in fire, SPUP became the first University and Catholic University in Cagayan Valley in 1982.</h4>

		<h4><img src="image/history3.jpg" alt="" style="height: 100px;width: 200px;float:left;display:block; margin-left: auto; margin-right: 5px;">
		In its efforts towards global and international education, SPUP gained the distinction of being the First Private Catholic University in Asia and the First Private University in the Philippines to be granted ISO 9001 Certification by TUV Rheinland in 2000 and the only University accredited by the Asian Association of Schools of Business International (AASBI) in 2014. SPUP has also been selected, as one of only 11 Universities in the Philippines, by World Bank as Knowledge for Development Center (KDC). Moreover the Commission on Higher Education (CHED) has designated SPUP as one of the 12 participating schools in the entire country for the ASEAN International Mobility for Students (AIMS) programme. In July 2014, SPUP was conferred a full accredited status by the International Accreditation Organization (IAO) in recognition of its outstanding organizational management, business management and business performance through its commitment to quality and continuous improvement.
		SPUP has also been sought as a partner University of international schools in Asia, Europe and Australia. These accolades advanced the stature of SPUP as an International University. Currently, the international community in SPUP, comprising of international and exchange students and professors, is continuously growing; while, its international linkages and partnerships are extensively expanding. Subsequently the pioneering Internationalization initiatives of SPUP have fostered opportunities for SPUP students

		<img src="image/history4.jpg" alt="" style="height: 100px;width: 200px;float:right;display:block; margin-left: auto; margin-right: auto;">and teachers for their academic exposure and exchange, work and travel, and cultural immersions in various countries across the world. As a result, SPUP has been recognized as an affiliate of the UNESCO Associated Schools Project Network, or ASPNet for its support to international understanding, peace, intercultural dialogue, sustainable development and quality education in practice.</h4>

		<h4><img src="image/history5.jpg" alt="" style="height: 100px;width: 200px;float:left;display:block; margin-left: auto; margin-right: 5px;">
		In the evangelization of faith and promotion of arts and culture, the Catholic Bishops’ Conference of the Philippines through the Episcopal Commission on Culture (CBCP-ECC) identified SPUP as a Catholic Center for Culture in 2012. Through this designation, SPUP is tasked to collaborate in the conservation of the patrimony of the Church and to promote greater awareness of the Church’s heritage through education, worship and the Sacraments. SPUP has also instituted a proactive ecumenical approach towards intercultural and interfaith differences due to the increasing population of international students. The creative approach “to where faith and culture meet” responds to the challenges of internationalization where the academic community is strongly rooted in the Catholic beliefs, respectful of others’ creed and culture. In 2014, the National Historical Commission (NHCP) of the Philippines bestowed SPUP a Historical Marker in recognition of its contribution in the historical development of Cagayan Valley in the fields of education and public service. The recognition endeavored SPUP one of the government-recognized historical landmarks in the region and in the country.</h4>

		<h4><img src="image/history6.jpg" alt="" style="height: 100px;width: 200px;float:right;display:block; margin-left: auto; margin-right: auto;">
		oday, SPUP is recognized by the Commission on Higher Education (CHED) as an Autonomous Institution; Center of Excellence in Nursing, Center of Excellence in Teacher Education; Center of Development in Information Technology; Center of Teacher Training Institution; the first accredited Integrated Basic Education Unit in Cagayan Valley; a deputized institution for the Expanded Tertiary Education Equivalency Accreditation Program (ETEAAP); Most Outstanding Student Services in the Philippines; Outstanding Research/Academic Library in the Philippines; and, the Lead School of the St. Paul University System.

		For more than a century of existence, SPUP has proven its tradition of excellence and untarnished reputation of providing quality, Catholic, Paulinian education. As it offers basic education to graduate school programs, SPUP has continuously form graduates in personifying Catholic and Paulinian values and virtues; thereby, making a difference in contributing to social transformation in the service of the nation, the Church and the entire humanity.</h4>
		</div>
	</div>

	<div class="container">
		<div class="footer">
	 	 	<div class="jumbotron">
	 	 	<p style="text-align: center;margin-top: -40px"> Accreditations </p>
	 	 	<div class="row">
	 		 	<div class="col-sm-2" style="margin-right: 35px"><img src="image/iso.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">ISO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/iao.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">IAO</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/aasbi.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">AASBI</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/paascu.jpg" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PAASCU</h4>
	    		</div>

	    		<div class="col-sm-2" style="margin-right: 35px"><img src="image/pacucoa.png" alt="" style="height: 50px;width: 100px;float:middle;display:block; margin-left: auto; margin-right: auto;">
	    			<h4 style="text-align: center">PACUCOA</h4>
	    		</div>
	  		</div>	
	</div>


</body>
</html>